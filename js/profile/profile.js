$(document).ready(function () {
    $('#image1').change(function (event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $(".profile-background").css('background-image', 'url(' + URL.createObjectURL(event.target.files[0]) + ')');
    });

    $('#image2').change(function (event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $(".profile").css('background-image', 'url(' + URL.createObjectURL(event.target.files[0]) + ')');
        $('#posted').attr('src',tmppath);
        $('#already-post').attr('src',tmppath);
    }); 

    $('#Edit-profile').mouseenter(function () {
        $(this).css('padding', '10px');
        // $(this).css(' transition',' 5s 0s');
        $(this).css('color', 'red');
        $(this).css('backgroundColor', '#e7f5f2');
    });
    // $('#Edit-profile').click(function(){
    //     alert('yes');
    //     $('body').append($('.model-boy'));
    // })
    $('#Edit-profile').mouseleave(function () {
        $(this).css('padding', '0px');
        $(this).css('color', 'blue');
        $(this).css('backgroundColor', 'transparent');
    });

    $('.profile-background').mouseenter(function () {
        $(this).css('cursor', 'pointer');
    });

    $('.profile-background').click(function () {
        setData();
    });

    $('#image2').mouseenter(function () {
        $('.change-profile').css('backgroundColor', '#428BCA')
        $('.change-profile').css('boxShadow', 'inset 0 0 0 2px #3071A9');
        $('.change-profile').css('color', 'white')
    });

    $('#image2').mouseleave(function () {
        $('.change-profile').css('backgroundColor', 'rgba(212, 206, 206, 0.8)')
        $('.change-profile').css('boxShadow', 'inset 0 0 0 0px rgba(212, 206, 206, 0.8)');
        $('.change-profile').css('color', 'blue')
    });

    $('.current-info').mouseenter(function(){
        $('.profile-info').css('backgroundColor','#333366a2');
        $('.profile-info').css('color','white');
        $('#Edit-profile').css('color','white');
        $('.change-profile').css('color', '#930077')
    });

    $('.current-info').mouseleave(function(){
        $('.profile-info').css('backgroundColor','#90aeffbd');
        $('.profile-info').css('color','black');
        $('.change-profile').css('color', 'black')
        $('#Edit-profile').css('color','black');
    });

    var name = $('#name');
    var gender = $('#gender');
    var education = $('#education');
    var phone = $('#phone');
    var email = $('#email');
    //b=birth of date
    //c=current of date
    var b_villege = $('#b-villege');
    var b_communt = $('#b-communt');
    var b_district = $('#b-district');
    var b_province = $('#b-province');

    var c_villege = $('#c-villege');
    var c_communt = $('#c-communt');
    var c_district = $('#c-district');
    var c_province = $('#c-province');

    $('#Update-info').click(function () {
        document.getElementById("tbl").rows[0].cells[1].innerHTML = name.val();
        document.getElementById("tbl").rows[1].cells[1].innerHTML = gender.val();
        document.getElementById("tbl").rows[2].cells[1].innerHTML = 'ភូមិ​ ' + b_villege.val() + " ឃុំ " + b_communt.val() + " ស្រុក " + b_district.val() + " ខេត​្ត " + b_province.val();
        document.getElementById("tbl").rows[3].cells[1].innerHTML = 'ភូមិ​ ' + c_villege.val() + " ឃុំ " + c_communt.val() + " ស្រុក " + c_district.val() + " ខេត្ត " + c_province.val();
        document.getElementById("tbl").rows[4].cells[1].innerHTML = education.val();
        document.getElementById("tbl").rows[5].cells[1].innerHTML = phone.val();
        document.getElementById("tbl").rows[6].cells[1].innerHTML = email.val();
    });
    function getData() {
        var name = $('#name').val();
        var gender = $('#gender').val();
        var education = $('#education').val();
        var phone = $('#phone').val();
        var email = $('#email').val();
        var table = $('#tbl')
    }

    function setData() {
        var n = document.getElementById("tbl").rows[0].cells[1].innerHTML;
        var g = document.getElementById("tbl").rows[1].cells[1].innerHTML;
        var b = document.getElementById("tbl").rows[2].cells[1].innerHTML;
        var c = document.getElementById("tbl").rows[3].cells[1].innerHTML;
        var e = document.getElementById("tbl").rows[4].cells[1].innerHTML;
        var p = document.getElementById("tbl").rows[5].cells[1].innerHTML;
        var em = document.getElementById("tbl").rows[6].cells[1].innerHTML;
        $('#name').val(n);
        $('#gender').innerHTML = g;
        $('#gender').val(g);
        $('#education').val(e)
        $('#phone').val(p);
        $('#email').val(em);
        var bi, cu = new Array();
        bi = b.split(" ");
        $('#b-villege').val(bi[1]);
        $('#b-communt').val(bi[3]);
        $('#b-district').val(bi[5]);
        $('#b-province').val(bi[7]);
        var cu = c.split(" ");
        $('#c-villege').val(bi[1]);
        $('#c-communt').val(bi[3]);
        $('#c-district').val(bi[5]);
        $('#c-province').val(bi[7]);

    }
    function Errors(){
        var at=email.val().indexOf("@");
        var aot=email.val().lastIndexOf(".");
        var ms_email=$('#lemail');
        if(at<1||dot<at+2||dot+2>=email.val().length){
            ms_email.text('Wrong email formate');
        }
    }
    
})