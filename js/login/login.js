$(document).ready(function(){
    //Add Year
    select=document.getElementById('pSCY');
    sSCY=document.getElementById('sSCY');
    hSCY=document.getElementById('hSCY')
    for(var i=2018;i>=1970;i--){
        var opt=document.createElement('option');
        var optsSCY=document.createElement('option')
        var opthSCY=document.createElement('option');
        optsSCY.value=i;
        optsSCY.innerHTML=i;
        opthSCY.value=i;
        opthSCY.innerHTML=i;
        opt.value=i;
        opt.innerHTML=i;
        select.appendChild(opt);
        sSCY.appendChild(optsSCY);
        hSCY.appendChild(opthSCY);
    }

    //Add City
    var city=document.getElementById('city');
    var opt=document.createElement('option');
    opt.value="pp";
    opt.innerHTML="ភ្នំពេញ";
    city.appendChild(opt);

    //Add Sangkhat
    var sangkhat=document.getElementById('sangkhat');
    var optSangKhat=document.createElement('option');
    optSangKhat.value="toulkork";
    optSangKhat.innerHTML="ទួលគោក";
    sangkhat.appendChild(optSangKhat);
    var optSangKhat1=document.createElement('option');
    optSangKhat1.value="stungmeanchey";
    optSangKhat1.innerHTML="ស្ទឹងមានជ័យ";
    sangkhat.appendChild(optSangKhat1);
    var optSangKhat2=document.createElement('option');
    optSangKhat2.value="beungtrobek";
    optSangKhat2.innerHTML="បឹងត្របែក";
    sangkhat.appendChild(optSangKhat2);
    
    //Add Primary School
    var pSchool=document.getElementById('pSC');
    var optPS=document.createElement('option');
    optPS.value="baktuk";
    optPS.innerHTML="បឋមសិក្សាបាក់ទួក"
    pSchool.appendChild(optPS);
    
    //Add Secondary School
    var sSchool=document.getElementById('sSC');
    var optSC=document.createElement('option');
    optSC.value="sonthormuk";
    optSC.innerHTML="អនុវិទ្យាល័យសន្ធោមុខ";
    sSchool.appendChild(optSC);

    //Add High School
    var hSchool=document.getElementById('hSC');
    var optHC=document.createElement('option');
    optHC.value="sonthormuk";
    optHC.innerHTML="វិទ្យាល័យសន្ធោមុខ";
    hSchool.appendChild(optHC);

    
    
})